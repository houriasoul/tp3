package com.example.tp3;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class CityActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView Name, Country, Temperature, Humdity, Wind, Cloudiness, LastUpdate;
    private ImageView image;
    private City city;
    private SwipeRefreshLayout Refresh;
    WeatherDbHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        Intent in= getIntent();
        dbHelper=new WeatherDbHelper(this);
        Bundle extras = getIntent().getExtras();

        city = (City)extras.get("actkey");

        Name = (TextView) findViewById(R.id.nameCity);
        Country = (TextView) findViewById(R.id.country);
        Temperature = (TextView) findViewById(R.id.editTemperature);
        Humdity = (TextView) findViewById(R.id.editHumidity);
        Wind = (TextView) findViewById(R.id.editWind);
        Cloudiness = (TextView) findViewById(R.id.editCloudiness);
        LastUpdate = (TextView) findViewById(R.id.editLastUpdate);
        image = (ImageView) findViewById(R.id.imageView);

        Refresh = findViewById(R.id.refresh_layout_city);
        Refresh.setOnRefreshListener(this);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (MainActivity.isOnline(CityActivity.this)) {
                    Refresh.setRefreshing(true);
                    new UpdateCityWheather().execute();

                } else {
                    Snackbar.make(findViewById(R.id.refresh_layout_city), "Vérifier la connecxion", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            }
        });

    }

    @Override
    public void onRefresh() {
        if (MainActivity.isOnline(CityActivity.this)) {
            new UpdateCityWheather().execute();

        } else {
            Snackbar.make(findViewById(R.id.refresh_layout_city), "Vérifier la connecxion", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }


    }

    private void updateView() {

        Name.setText(city.getName());
        Country.setText(city.getCountry());
        Temperature.setText(city.getTemperature()+" °C");
        Humdity.setText(city.getHumidity()+" %");
        Wind.setText(city.getFullWind());
        Cloudiness.setText(city.getHumidity()+" %");
        LastUpdate.setText(city.getLastUpdate());

        if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
            Log.d(TAG,"icon="+"icon_" + city.getIcon());
            image.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
            image.setContentDescription(city.getDescription());
        }

    }

    @Override
    public void onBackPressed() {
        System.out.println("back");
        Intent intent = new Intent();
        intent.putExtra(MainActivity.SELECTED_CITY, city);
        setResult(Activity.RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }


    class UpdateCityWheather extends AsyncTask<Object, Integer, City> {
        @Override
        protected City doInBackground(Object... objects) {

            HttpURLConnection Connex = null;
            try {
                // prepare url
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                // send a GET request to the serve
                Connex = (HttpURLConnection) urlToRequest.openConnection();
                Connex.setRequestMethod("GET");
                Connex.connect();
                // read data
                InputStream inputStream = Connex.getInputStream();
                JSONResponseHandler Handler = new JSONResponseHandler(city);
                Handler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                Connex.disconnect();
            }
            return city;
        }

        @Override
        protected void onPostExecute(City city) {
            updateView();
            dbHelper.updateCity(city);
            Refresh.setRefreshing(false);
            super.onPostExecute(city);

        }
    }



}
