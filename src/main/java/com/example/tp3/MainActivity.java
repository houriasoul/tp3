package com.example.tp3;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<City> listcity;
    WeatherDbHelper dbHelper;
    MyRecyAdapter recyAdapter;
    RecyclerView recycle;
    private SwipeRefreshLayout Refresh;
    public static final String SELECTED_CITY = "SELECTED_CITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Refresh = findViewById(R.id.refresh_layout);
        Refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                System.out.println("Hello");
                new UpdateCitiesWheather().execute();
                if (MainActivity.isOnline(MainActivity.this)) {
                    new UpdateCitiesWheather().execute();

                } else {
                    Snackbar.make(findViewById(R.id.refresh_layout), "Vérifiez la connection", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                recycle.setAdapter(new MyRecyAdapter(null));
            }

        });
        FloatingActionButton fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.print("You found me here ");
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, 2);
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        recycle = (RecyclerView) findViewById(R.id.recy) ;
        dbHelper=new WeatherDbHelper(this);
        dbHelper.populate();
        listcity= new ArrayList<City>();
        listcity=dbHelper.getAllCities();
        recycle.setLayoutManager(new LinearLayoutManager(this));
        recycle.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyAdapter= new MyRecyAdapter(listcity);
        setUpItemTouchHelper();
        recycle.setAdapter(recyAdapter);


    }
    private void setUpItemTouchHelper() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int Position = viewHolder.getAdapterPosition();
                City city=listcity.get(Position);
                int id =(int) city.getId();
                dbHelper.deleteCity(id);
                recyAdapter.remove(Position);
            }

        };
        ItemTouchHelper THelper = new ItemTouchHelper(simpleItemTouchCallback);
        THelper.attachToRecyclerView(recycle);
    }
    public static boolean isOnline(Context context) {
        ConnectivityManager conect = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conect.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (1 == requestCode && RESULT_OK == resultCode) {
            City update = data.getParcelableExtra(MainActivity.SELECTED_CITY);
            dbHelper.updateCity(update);
            listcity=dbHelper.getAllCities();
            recyAdapter.listcitys=listcity;
            recyAdapter.notifyDataSetChanged();
            recycle.setAdapter(new MyRecyAdapter(listcity));
            System.out.println("City :" + update.getName() + " Country :" + update.getCountry());
        }
        if (2 == requestCode && RESULT_OK == resultCode) {
            City nouvCity = data.getParcelableExtra(NewCityActivity.BUNDLE_EXTRA_NEW_CITY);
            dbHelper.addCity(nouvCity);
            listcity=dbHelper.getAllCities();
            recyAdapter.listcitys=listcity;
            recyAdapter.notifyDataSetChanged();
            recycle.setAdapter(new MyRecyAdapter(listcity));
            System.out.println("City :" + nouvCity.getName() + " Country :" + nouvCity.getCountry());

        }
    }

    class MyRecyAdapter extends RecyclerView.Adapter<RowHolder>{
        private List<City> listcitys;
        City city;
        public MyRecyAdapter(List<City> citie){
            listcitys=citie;
        }
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater().inflate(R.layout.row,parent,false)));
        }

        @Override
        public void onBindViewHolder(@NonNull final RowHolder holder, int position) {

            city=listcitys.get(position);
            holder.bindModel(listcity.get(position));
            holder.Name.setText(city.getName());
            holder.Country.setText(city.getCountry());

            holder.image.setImageResource(R.drawable.icon_01d);
            holder.temperature.setText(city.getTemperature()+" °C");
            holder.citye=city;
            holder.var.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), CityActivity.class);
                    Bundle bundle = new Bundle();
                    intent.putExtra("actkey",holder.citye);
                    ((Activity)v.getContext()).startActivityForResult(intent,1);
                }
            });
            if (city.getIcon()!=null && !city.getIcon().isEmpty()) {
                holder.image.setImageDrawable(getResources().getDrawable(getResources()
                        .getIdentifier("@drawable/"+"icon_" + city.getIcon(), null, getPackageName())));
                holder.image.setContentDescription(city.getDescription());
            }


        }

        @Override
        public int getItemCount() {
            return (listcity.size());
        }
        public void remove(int position) {
            if (position < 0 || position >= listcitys.size()) {
                return;
            }
            listcitys.remove(position);
            notifyItemRemoved(position);
            notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    class UpdateCitiesWheather extends AsyncTask<Object, Integer, List<City>> {
        private List<City> HCity;

        @Override
        protected void onPreExecute() {
            System.out.println("You found me here");
            HCity = dbHelper.getAllCities();
            recyAdapter.listcitys=HCity;
            recyAdapter.notifyDataSetChanged();
            recycle.setAdapter(recyAdapter);

            super.onPreExecute();
        }

        @Override
        protected List<City> doInBackground(Object... objects) {
            HttpURLConnection Connex = null;
            HCity = dbHelper.getAllCities();
            for (City city : HCity) {

                try {
                    URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                    Connex = (HttpURLConnection) urlToRequest.openConnection();
                    Connex.setRequestMethod("GET");
                    Connex.connect();
                    InputStream inputStream = Connex.getInputStream();
                    JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                    jsonHandler.readJsonStream(inputStream);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Connex.disconnect();
                }
                dbHelper.updateCity(city);
            }
            return HCity;
        }


        @Override
        protected void onPostExecute(List<City> cities) {
            super.onPostExecute(cities);
            Refresh.setRefreshing(false);


        }

        private void weatherRequest(City city) {
            HttpURLConnection Connex = null;
            try {

                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                Connex = (HttpURLConnection) urlToRequest.openConnection();
                Connex.setRequestMethod("GET");
                Connex.connect();
                InputStream inputStream = Connex.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                Connex.disconnect();
            }

        }
    }
}
