package com.example.tp3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class NewCityActivity extends AppCompatActivity {

    private EditText Name, Country;
    public static final String BUNDLE_EXTRA_NEW_CITY = "BUNDLE_EXTRA_NEW_CITY";
    WeatherDbHelper dbHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);
        Name = (EditText) findViewById(R.id.editNewName);
        Country = (EditText) findViewById(R.id.editNewCountry);
        final Button but = (Button) findViewById(R.id.button);
        dbHelper=new WeatherDbHelper(this);
        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                City city = new City(Name.getText().toString(), Country.getText().toString());
                dbHelper.addCity(city);
                Intent intent = new Intent();
                intent.putExtra(BUNDLE_EXTRA_NEW_CITY, city);
                setResult(RESULT_OK, intent);

                finish();
            }
        });
    }

}
