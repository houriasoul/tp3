package com.example.tp3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    City citye;
    TextView Name = null;
    TextView Country = null;
    TextView temperature = null;
    ImageView image = null;
    ConstraintLayout var;
    public RowHolder(View row) {
        super(row);
        Name = (TextView)row.findViewById(R.id.cName);
        Country = (TextView)row.findViewById(R.id.cCountry);
        temperature = (TextView)row.findViewById(R.id.temperature);
        image = (ImageView)row.findViewById(R.id.imageViewRow);

        var = (ConstraintLayout)row.findViewById(R.id.rowrecy);


    }


    @Override
    public void onClick(View v) {

    }
    void bindModel(City city){


    }

}
